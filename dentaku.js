var temp="",operand1="",operand2="",operator="",counter=0,dotFlag=false,equalFlag=false;
function operate(var1,var2,operator){ //演算の核になる部分。
    if (operator=="+"){
        return parseFloat(var1) + parseFloat(var2);
    }
    if (operator=="-"){
        return parseFloat(var1) - parseFloat(var2);
    }
    if (operator=="*"){
        return parseFloat(var1) * parseFloat(var2);
    }
    if (operator=="/"){
        if (var2=="0"){
            return "ERROR";
        }
        else{
            return parseFloat(var1) / parseFloat(var2);
        }
    }
}
function cut(var1){ //桁溢れの処理、上手くいかない。
    var counter=0,temp=0;
    for(temp=var1;counter<8;counter++){
        if(temp>=10000000){
            temp = var1.toFixed(counter)
            counter=8;
        }
        else{
            temp = temp * 10;
            counter++;
        }
    }
    if((''+temp)>=100000000){
        return "ERROR";
    }
    else{
        return temp;
    }
}
function hitkey(var1){ //キーが押された場合の処理。
    if(var1=="C"|var1=="c"){
        temp = "0";
        document.dentaku.screen.value = temp;
        temp = "";
        counter = 0;
        operator = "";
        operand1 = "";
        operand2 = "";
        equalFlag=false;
        dotFlag=false;
    }
    else if(temp=="ERROR"){
    }
    else if(var1=="="){
        
        if (operator==""){
            
        }
        else{
            if(temp==""){
                temp="0";
            }
            var swap;
            swap = operate(operand1,temp,operator);
            cut(swap);
            document.dentaku.screen.value = swap;
            operand1 = swap;
            equalFlag = true;
            if (swap=="ERROR"){
                temp = swap;           
            }
        }
    }
    else if(var1=="+"|var1=="-"|var1=="*"|var1=="/"){
        var swap=temp;
        if(equalFlag){
            swap=operand1;
        }
        else if(operator!=""){
            swap = operate(operand1,temp,operator);
            cut(swap);
            document.dentaku.screen.value = swap;
        }
        operand1 = swap;
        temp = "";
        counter = 0;
        operator = var1;
        dotFlag=false;
        equalFlag=false;
    }
    else if(var1=="+/-"){
        if(!equalFlag){
            temp = 0 - temp;
            document.dentaku.screen.value = temp;
        }
    }
    else if(var1=="%"){
    }
    else if(var1=="."){
        if(dotFlag){
            
        }
        else {
            if(temp==""){
                temp = "0";
                counter++;
            }
            temp += var1;
            document.dentaku.screen.value = temp;
            dotFlag = true;
        }
    }
    else{
        if(counter<8){
            if(equalFlag){
                temp = "";
                operator = "";
                operand1 = "";
                operand2 = "";
                equalFlag = false;
                dotFlag = false;
                counter=0;
            }
            temp += var1;
            counter++;
            document.dentaku.screen.value = temp;
        }
    }
    

    return 0;
}
